# DrumKit with HTML CSS and JS

A simple drum kit using HTML, CSS and JS.
Feel free to use and share, as long as the author is mentioned. 
This is not developed any further and only bugfixes and security patches are provided. 
If you find any issues or bugs, please use the issue tracker. 